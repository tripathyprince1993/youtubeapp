const mongoose=require('mongoose');

const Video_Schema=mongoose.Schema({
   
    Title:{type:String,required:false},
    video_URL:{type:String,required:false},
    created_by:{type:String,required:false},
    creation_date:{type:Date,default:Date.now},
    updated_by:{type:String,required:false},
    updation_date:{type:Date,default:Date.now},
})
module.exports=mongoose.model("Videos",Video_Schema);